FROM registry.gitlab.com/islandoftex/images/texlive:latest
RUN \
  apt-get update && \
  apt-get install -qy python3-pip && \
  pip install -U \
      jupyter-book \
      jupytext \
      sphinx-proof \
      sphinx-inline-tabs \ 
      sphinx-exercise && \
  pip install -U \
      emcee \
      matplotlib \
      numpy \
      scipy \
      pandas
