# TIF385 Bayesian inference and machine learning
This repository contains the lecture notes for the Chalmers course "Bayesian inference and machine learning" in year 2 of the Engineering Physics program.

The course website can be accessed at: https://cforssen.gitlab.io/tif385-book/
